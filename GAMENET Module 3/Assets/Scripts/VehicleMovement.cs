using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    public float acceleration = 5.0f;
    public float deceleration = 10.0f;
    public float maxSpeed = 10.0f;
    public float rotationSpeed = 200.0f;

    private float currentSpeed = 0.0f;
    public bool isControlEnabled;

    private void Start()
    {
        isControlEnabled = true;
    }
    private void LateUpdate()
    {
        if (isControlEnabled)
        {
            float inputVertical = Input.GetAxis("Vertical");

            if (inputVertical != 0)
            {
                currentSpeed += inputVertical * acceleration * Time.deltaTime;
                currentSpeed = Mathf.Clamp(currentSpeed, -maxSpeed, maxSpeed);
            }
            else
            {
                float decelerationValue = Mathf.Sign(currentSpeed) * deceleration * Time.deltaTime;
                currentSpeed = Mathf.Clamp(currentSpeed - decelerationValue, 0, Mathf.Infinity);
            }
            
            float translation = currentSpeed * Time.deltaTime;
            transform.Translate(0, 0, translation);

            float rotation = Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;
            transform.Rotate(0, rotation, 0);
        }
    }

}

