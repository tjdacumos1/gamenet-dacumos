using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skills : MonoBehaviour
{
    public GameObject projectilePrefab;
    public string skillName;
    private float currentCooldownTime;
    public float cooldownTime;
    public bool isOnCooldown;
    public Transform projectileSpawnPoint;
    public float bulletSpeed;
    public float duration;
    public StatusAilments statusAilmentsEffect;

    private void Start()
    {
        isOnCooldown = false;
        ClassicRacingManager.instance.coolDownText.text = skillName;
        StartCoroutine(skillCooldownTimer());
    }
    private void Update()
    {
        if((Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) && ClassicRacingManager.instance.isGamePlaying)
        {
            if(!isOnCooldown)
            {
                ActivateSkill();
            }           
        }
    }

    protected virtual void ActivateSkill()
    {
        if (GetComponent<Status>().currentStatus != StatusAilments.Stunned)
        {
            StartCoroutine(skillCooldownTimer()); //can't use skill when stunned
        }
    }

    IEnumerator skillCooldownTimer()
    {
        isOnCooldown = true;
        currentCooldownTime = cooldownTime;
        ClassicRacingManager.instance.coolDownText.fontSize = 160;

        while (currentCooldownTime > 0)
        {
            ClassicRacingManager.instance.coolDownText.text = currentCooldownTime.ToString();
            
            float fillAmount = 1 - (currentCooldownTime / cooldownTime);
            ClassicRacingManager.instance.cooldownRadial.fillAmount = fillAmount;
            
            yield return new WaitForSeconds(1.0f);
            currentCooldownTime--;
        }

        ClassicRacingManager.instance.cooldownRadial.fillAmount = 1;
        ClassicRacingManager.instance.coolDownText.fontSize = 50;
        ClassicRacingManager.instance.coolDownText.text = skillName;
        isOnCooldown = false;
    }
}
