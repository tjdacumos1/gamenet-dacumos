using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

public enum AttackType
{
    Raycast,
    Projectile
}

public class Shooting : MonoBehaviourPunCallbacks
{
    public GameObject carModel;
    public GameObject crosshair;

    [Header("Stats")]
    public float currentHealth;
    public float startHealth = 100;
    public bool isDead;

    [Header("Shooting")]
    public Camera playerCamera;
    public AttackType attackType;
    public float wepDamage;
    public float reloadSpeed;
    private bool isReady;

    [Header("Raycast Shooting")]
    public GameObject hitEffectPrefab;
    public float laserMaxLength = 300f;
    public float maxLaserDuration = 3;
    public LineRenderer laserEffect;
    public bool isHitCooldown;
    private float currentLaserDuration;


    [Header("Projectile Shooting")]
    public GameObject projectilePrefab;
    public Transform projectileSpawnPoint;
    private int bulletCount;
    public int clipCapacity;
    public float fireRate;
    public float bulletSpeed;  

    private void Start()
    {
        currentHealth = startHealth;
        isReady = true;
        isDead = false;
        bulletCount = clipCapacity;
        currentLaserDuration = maxLaserDuration;
        laserEffect.enabled = false;
        isHitCooldown = false;
        crosshair.SetActive(photonView.IsMine);
    }

    private void Update()
    {
        if ((Input.GetMouseButton(0) || Input.GetKey(KeyCode.Space)) && isReady && !isDead)
        {
            if (photonView.IsMine)
            {
                Fire();
            }
        }
        else if (Input.GetMouseButtonUp(0) || Input.GetKeyUp(KeyCode.Space))
        {
            laserEffect.enabled = false;
        }
    }

    public void Fire()
    {
        laserEffect.SetPosition(0, projectileSpawnPoint.position);    
        Ray ray = playerCamera.ViewportPointToRay(new Vector3(0.5f, 0.45f));
        Vector3 laserOrigin = playerCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.45f, 0.0f));
        RaycastHit hit;
        
        if (attackType == AttackType.Raycast)
        {
            if (Physics.Raycast(ray, out hit, 200) && isReady)
            {
                laserEffect.enabled = true;
                laserEffect.SetPosition(1, hit.point);
                photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

                if (hit.collider != null)
                {
                    currentLaserDuration -= Time.deltaTime;

                    if (currentLaserDuration <= 0)
                    {
                        StartCoroutine(reload());
                    }

                    if ((hit.collider.gameObject.CompareTag("Player") || !hit.collider.gameObject.GetComponent<PhotonView>().IsMine) && !isHitCooldown)
                    {
                        hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, wepDamage);
                        StartCoroutine(fireRateTimer()); //dps control in this case
                        Debug.Log("Getting hit " + hit);
                    }
                    else
                    {
                        laserEffect.SetPosition(1, laserOrigin + (playerCamera.transform.forward * 200));
                        
                    }
                   
                }
            } 
        }
        
        if(attackType == AttackType.Projectile)
        {
            if (bulletCount > 0 && isReady)
            {
                StartCoroutine(fireRateTimer());
                GameObject projectileGameObject = PhotonNetwork.Instantiate(projectilePrefab.name, projectileSpawnPoint.position, projectileSpawnPoint.rotation);
                Rigidbody projectileRb = projectileGameObject.GetComponent<Rigidbody>();
                projectileRb.AddForce(projectileSpawnPoint.forward * bulletSpeed, ForceMode.Impulse);

                Projectile projectile = projectileGameObject.GetComponent<Projectile>();
                if (projectile != null)
                {
                    projectile.SetProjectileDamage(wepDamage);
                }

                bulletCount--;
            }
            else
            {
                StartCoroutine(reload());
            }
        }        
    }

    [PunRPC]
    public void TakeDamage(float damage, PhotonMessageInfo info)
    {
        this.currentHealth -= damage;
        Debug.LogWarning(info.photonView.Owner.NickName + " is getting hit by " + info.Sender.NickName);

        if (currentHealth <= 0)
        {
            Die(info.Sender.NickName);

            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void TakeDamageFromExternal(float damage)
    {
        gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, damage);
    }

    private void Die(string killer)
    {
        isDead = true;
        carModel.SetActive(false);
        gameObject.GetComponent<VehicleMovement>().enabled = false;
        gameObject.GetComponent<Rigidbody>().useGravity = false;
        gameObject.GetComponent<Collider>().enabled = false;
        isReady = false;

        if (photonView.IsMine)
        {
           
            string nickName = photonView.Owner.NickName;
            int viewId = photonView.ViewID;

            object[] data = new object[] { nickName, DeathRaceManager.instance.playersLeft, viewId, killer };

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };

            SendOptions sendOptions = new SendOptions
            {
                Reliability = false
            };

            PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoDiedEventCode, data, raiseEventOptions, sendOptions);
        }

    }

    private IEnumerator reload()
    {
        isReady = false;
        bulletCount = clipCapacity;
        currentLaserDuration = maxLaserDuration;
        laserEffect.enabled = false;

        yield return new WaitForSeconds(reloadSpeed);

        isReady = true;
    }

    private IEnumerator fireRateTimer()
    {
        isReady = false;
        isHitCooldown = true;        
        yield return new WaitForSeconds(fireRate);
        isHitCooldown = false;
        isReady = true;
    }

}
