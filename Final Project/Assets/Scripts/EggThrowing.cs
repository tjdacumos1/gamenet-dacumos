using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EggThrowing : Skills
{
    protected override void ActivateSkill()
    {
        base.ActivateSkill();

        int numberOfProjectiles = 5;
        float coneSpreadAngle = 45f;

        for (int i = 0; i < numberOfProjectiles; i++)
        {
            float normalizedAngle = i / (float)(numberOfProjectiles - 1); 
            float angle = normalizedAngle * coneSpreadAngle - coneSpreadAngle / 2f;

            Quaternion rotation = Quaternion.Euler(0f, angle, 0f) * projectileSpawnPoint.rotation;

            GameObject projectileGameObject = PhotonNetwork.Instantiate(projectilePrefab.name, projectileSpawnPoint.position, rotation);           
            Rigidbody projectileRb = projectileGameObject.GetComponent<Rigidbody>();
            projectileRb.AddForce(projectileGameObject.transform.forward * bulletSpeed, ForceMode.Impulse);

            Projectile projectile = projectileGameObject.GetComponent<Projectile>();
            if (projectile != null)
            {
                projectile.SetProjectileEffect(statusAilmentsEffect,duration);
            }
        }
    }
}
