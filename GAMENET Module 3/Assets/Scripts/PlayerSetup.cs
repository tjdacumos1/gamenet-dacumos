using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera playerCam;
    
    void Awake()
    {
        this.playerCam = transform.Find("Camera").GetComponent<Camera>();

        if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {            
            GetComponent<LapController>().enabled = photonView.IsMine;
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;            
        }

        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<LapController>().enabled = false;
            GetComponent<CountdownManager>().enabled = false;
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<DeathController>().enabled = photonView.IsMine;
        }
        
        playerCam.enabled = photonView.IsMine;
    }
}
