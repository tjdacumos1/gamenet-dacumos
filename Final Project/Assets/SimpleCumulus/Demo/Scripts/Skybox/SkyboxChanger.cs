﻿using UnityEngine;
using UnityEngine.UI;

public class SkyboxChanger : MonoBehaviour
{
    [SerializeField]
    private Dropdown _dropdown;

    public Material[] Skyboxes;

    //public void Awake()
    //{
    //    _dropdown = GetComponent<Dropdown>();
    //}

    public void Start()
    {
        RenderSettings.skybox = Skyboxes[1];
        RenderSettings.skybox.SetFloat("_Rotation", 0);
    }
    public void ChangeSkybox()
    {
        RenderSettings.skybox = Skyboxes[3];
        RenderSettings.skybox.SetFloat("_Rotation", 0);
    }

    public void NextSkybox()
    {
        _dropdown.value = (_dropdown.value < Skyboxes.Length - 1) ? _dropdown.value + 1 : _dropdown.value = 0;
        ChangeSkybox();
    }
}