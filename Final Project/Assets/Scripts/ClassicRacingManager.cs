using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//I decided late that I'll be using this for the normal mode too
public class ClassicRacingManager : MonoBehaviourPunCallbacks
{
    [Header("System")]
    public GameObject[] playerPrefabs;
    public Transform[] startingPositions;
    public GameObject[] finisherTextUi;
    public GameObject ExitGameButton;

    [Header("Map/Race")]
    public Text checkpointTextUi;
    public Text lapTextUi;
    public int totalLapCount;
    public Text timeText;
    public Text winnerText;
    public int totalPlayerCount;
    public bool isGamePlaying;
    

    //normal mode only
    [Header("Skill Related things")]
    public GameObject cooldownUi;
    public Image cooldownRadial;
    public Text coolDownText;
    public Text status;

    public static ClassicRacingManager instance = null;

    public List<GameObject> lapTriggers = new List<GameObject>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        
    }
    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;
            object numberOfLaps;

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
                Quaternion instantiateRotation = Quaternion.Euler(0f, 180f, 0f);

                PhotonNetwork.Instantiate(playerPrefabs[(int)playerSelectionNumber].name, instantiatePosition, instantiateRotation);
            }

            if (PhotonNetwork.CurrentRoom.CustomProperties.TryGetValue("lc", out numberOfLaps))
            {
                totalLapCount = (int)numberOfLaps;                
            }

            totalPlayerCount = PhotonNetwork.CurrentRoom.PlayerCount;

            if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
            {
                cooldownRadial.fillAmount = 1;
                cooldownUi.SetActive(true);
                status.text = " ";
            }
            
            if(coolDownText == null || cooldownRadial == null || cooldownUi == null || status == null)
            {
                Debug.Log("not included in Classic Race");
            }
            
        }

        ExitGameButton.SetActive(false);   

        foreach (GameObject go in finisherTextUi)
        {
            go.SetActive(false);
        }

        if(PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;
        }
    }

    public void ExitToMainLobby()
    {
        if(PhotonNetwork.InRoom)
        {
            PhotonNetwork.LeaveRoom();
        }
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel("LobbyScene");
    }
}
