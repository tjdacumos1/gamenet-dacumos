using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.Pool;
using UnityEngine.UI;
using System;
using UnityEngine.Rendering;

public class LapController : MonoBehaviourPunCallbacks
{
    public List<GameObject> lapTriggers = new List<GameObject>();

    public enum RaiseEventsCode
    {
        WhoFinishedEventCode = 0,
        GameFinishedEventCode = 1
    }

    private int finishOrder = 0;
    private int currentCheckpoint = 0;
    private int currentLap = 1;
    private bool isFinished;
    private bool isEndingTimerStarted;
    private string firstPlaceFinisherName;

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code == (byte)RaiseEventsCode.WhoFinishedEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfFinishedPlayer = (string)data[0];
            finishOrder = (int)data[1];
            int viewId = (int)data[2];

            Debug.Log(nickNameOfFinishedPlayer + " " + finishOrder);

            if(string.IsNullOrEmpty(firstPlaceFinisherName))
            {
                firstPlaceFinisherName = nickNameOfFinishedPlayer;
            }

            GameObject orderUiText = ClassicRacingManager.instance.finisherTextUi[finishOrder - 1];
            orderUiText.SetActive(true);

            if (viewId == photonView.ViewID) //this is you
            {
                orderUiText.GetComponent<Text>().text = finishOrder + " " + nickNameOfFinishedPlayer + " (YOU)";
                orderUiText.GetComponent<Text>().color = Color.red;

                Debug.Log(finishOrder + " " + nickNameOfFinishedPlayer + " (YOU)");
                
            }
            else
            {
                orderUiText.GetComponent<Text>().text = finishOrder + " " + nickNameOfFinishedPlayer;

                if (!isEndingTimerStarted)
                {
                    isEndingTimerStarted = true;
                    StartCoroutine(EndingTimer());
                }
            }
            CheckIfLastPlayerEnded();
        }

        if(photonEvent.Code == (byte)RaiseEventsCode.GameFinishedEventCode)
        {
            ClassicRacingManager.instance.ExitGameButton.SetActive(true);
            ClassicRacingManager.instance.isGamePlaying = false;

            if(finishOrder == 1 && isFinished)
            {
                ClassicRacingManager.instance.winnerText.text = "Congratulations " + firstPlaceFinisherName + "\n You finished 1st!!!";
                ClassicRacingManager.instance.winnerText.color = Color.green;
            }
            else
            {
                ClassicRacingManager.instance.winnerText.text = firstPlaceFinisherName + " wins!" + "\n" + photonView.Owner.NickName + " better luck next time!";
                ClassicRacingManager.instance.winnerText.color = Color.red;
            }

            Cursor.lockState = CursorLockMode.Confined;
        }
    }

    void Start()
    {
        isFinished = false;
        isEndingTimerStarted = false;
        firstPlaceFinisherName = null;

        foreach (GameObject go in ClassicRacingManager.instance.lapTriggers)
        {
            lapTriggers.Add(go);
        }
        ActivateCurrentCheckpoint();
        UpdateProgressUi();
    }

    private void OnTriggerEnter(Collider col)
    {
        if(lapTriggers.Contains(col.gameObject))
        {
            int indexOfTrigger = lapTriggers.IndexOf(col.gameObject);

            if(indexOfTrigger == lapTriggers.Count - 1)
            {
                currentLap++;
                currentCheckpoint = 0;
                checkIfPlayerFinished();
                ActivateCurrentCheckpoint();
                UpdateProgressUi();

            }    
            else if(indexOfTrigger == currentCheckpoint)
            {
                currentCheckpoint++;
                UpdateProgressUi();
                ActivateCurrentCheckpoint();
            }
        }      
    }

    private void UpdateProgressUi()
    {
        ClassicRacingManager.instance.checkpointTextUi.text = "Checkpoint: " + currentCheckpoint + " / " + lapTriggers.Count;        
        ClassicRacingManager.instance.lapTextUi.text = "Lap: " + currentLap + " / " + ClassicRacingManager.instance.totalLapCount;
        if(currentLap > ClassicRacingManager.instance.totalLapCount)
        {
            ClassicRacingManager.instance.lapTextUi.text = "Finished!";
            ClassicRacingManager.instance.checkpointTextUi.text = "Position No: " + finishOrder;
        }
    }

    private void checkIfPlayerFinished()
    {
        if(currentLap > ClassicRacingManager.instance.totalLapCount)
        {
            GameFinish();
        }
    }

    private void ActivateCurrentCheckpoint()
    {
        foreach(GameObject t in lapTriggers)
        {
            if(lapTriggers.IndexOf(t) == currentCheckpoint && !isFinished)
            {
                t.SetActive(true);
            }
            else
            {
                t.SetActive(false);
            }
        }
    }

    private void CheckIfLastPlayerEnded()
    {
        if(finishOrder >= ClassicRacingManager.instance.totalPlayerCount)
        {
            EndGame();
        }
    }

    public void EndGame()
    {
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.GameFinishedEventCode, null, raiseEventOptions, sendOptions);
    }

    public void GameFinish()
    {
        GetComponent<PlayerSetup>().playerCam.transform.parent = null;
        GetComponent<PlayerMovement>().enabled = false;
        GetComponent<Skills>().enabled = false;

        isFinished = true;
        finishOrder++;      

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        //event data
        object[] data = new object[] { nickName, finishOrder, viewId };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoFinishedEventCode, data, raiseEventOptions, sendOptions);
    }

    IEnumerator EndingTimer()
    {
        float timeToEndRace = 60.0f;

        while (!isFinished && timeToEndRace > 0)
        {
            timeToEndRace--;            
            ClassicRacingManager.instance.timeText.text = "Game Ending in " + timeToEndRace.ToString();
            yield return new WaitForSeconds(1f);
        }
        ClassicRacingManager.instance.timeText.text = " ";

        if (isFinished)
        {
            CheckIfLastPlayerEnded();
        }

        if(timeToEndRace <= 0)
        {
            EndGame();
        }
    }    
}
