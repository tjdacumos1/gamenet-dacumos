﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public enum RacingMode
{
    Racing,
    DeathRace
}
public class PlayerSelection : MonoBehaviour
{
    public GameObject[] SelectablePlayers;
    public int PlayerSelectionNumber;

    public Text musclePropertyText;
    public Text tunerPropertyText;
    public Text sportsPropertyText;

    void Start()
    {
        PlayerSelectionNumber = 0;

        ActivatePlayer(PlayerSelectionNumber);
    }

    private void ActivatePlayer(int x)
    {
        foreach (GameObject go in SelectablePlayers) {
            go.SetActive(false);
        }

        SelectablePlayers[x].SetActive(true);

        //setting the player selection for the vehicle 
        ExitGames.Client.Photon.Hashtable playerSelectionProperties = new ExitGames.Client.Photon.Hashtable() {{Constants.PLAYER_SELECTION_NUMBER, PlayerSelectionNumber}};
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerSelectionProperties);
    }

    public void goToNextPlayer()
    {
        PlayerSelectionNumber++;

        if(PlayerSelectionNumber >= SelectablePlayers.Length) {
            PlayerSelectionNumber = 0;
        }
        
        ActivatePlayer(PlayerSelectionNumber);
    }

    public void goToPreviousPlayer()
    {
        PlayerSelectionNumber--;

        if(PlayerSelectionNumber < 0) {
            PlayerSelectionNumber = SelectablePlayers.Length - 1;
        }

        ActivatePlayer(PlayerSelectionNumber);
    }

    public void SetRacingMode(RacingMode racingMode)
    {
        if(racingMode == RacingMode.Racing)
        {
            musclePropertyText.text = "Typical American Muscle Car";
            tunerPropertyText.text = "It's a GTR!!!";
            sportsPropertyText.text = "Car for flexing wealth";
        }

        if(racingMode == RacingMode.DeathRace)
        {
            musclePropertyText.text = "RPG - 45 damage, Ammo - 2, FireRate - Slow";
            tunerPropertyText.text = "Laser - 5 damage, Ammo - 3s, FireRate - Medium";
            sportsPropertyText.text = "AR - 4 Damage , Ammo - 50, Fire Rate - Fast";
        }
    }
}
