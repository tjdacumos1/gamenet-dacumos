using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : Skills
{
    protected override void ActivateSkill()
    {
        base.ActivateSkill();

        if(GetComponent<Status>().currentStatus == StatusAilments.Normal)
        {
            GetComponent<Status>().SetStatusAilment(statusAilmentsEffect, duration);
        }
        else if(GetComponent<Status>().currentStatus == StatusAilments.Slowed)
        {
            GetComponent<Status>().currentStatus = StatusAilments.Normal;
        }
    }
}
