using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class CountdownManager : MonoBehaviourPunCallbacks
{
    public Text timerText;

    public float timeToStartRace = 5.0f;

    void Awake()
    {
        timerText = ClassicRacingManager.instance.timeText;
        ClassicRacingManager.instance.isGamePlaying = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (PhotonNetwork.IsMasterClient && ClassicRacingManager.instance.isGamePlaying == false)
        {
            if (timeToStartRace > 0)
            {
                timeToStartRace -= Time.deltaTime;
                photonView.RPC("SetTime", RpcTarget.AllBuffered, timeToStartRace);
            }
            else if (timeToStartRace < 0)
            {
                photonView.RPC("StartRace", RpcTarget.AllBuffered);
            }
        }
    }

    [PunRPC]
    public void SetTime(float time)
    {
        if(time > 0)
        {
            timerText.text = "Game Starts in " + time.ToString("F1");
        }
        else
        {
            timerText.text = " ";
        }
    }

    [PunRPC]
    public void StartRace()
    {
        ClassicRacingManager.instance.isGamePlaying = true;
        GetComponent<CountdownManager>().enabled = false;
                   
    }
}
