﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public enum RacingMode
{
    Racing,
    DeathRace
}
public class PlayerSelection : MonoBehaviour
{
    public GameObject[] SelectablePlayers;
    public int PlayerSelectionNumber;

    public Text dragonPropertyText;
    public Text condorPropertyText;
    public Text chickenPropertyText;

    void Start()
    {
        PlayerSelectionNumber = 0;

        ActivatePlayer(PlayerSelectionNumber);
    }

    private void ActivatePlayer(int x)
    {
        foreach (GameObject go in SelectablePlayers) {
            go.SetActive(false);
        }

        SelectablePlayers[x].SetActive(true);

        //setting the player selection for the vehicle 
        ExitGames.Client.Photon.Hashtable playerSelectionProperties = new ExitGames.Client.Photon.Hashtable() {{Constants.PLAYER_SELECTION_NUMBER, PlayerSelectionNumber}};
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerSelectionProperties);
    }

    public void goToNextPlayer()
    {
        PlayerSelectionNumber++;

        if(PlayerSelectionNumber >= SelectablePlayers.Length) {
            PlayerSelectionNumber = 0;
        }
        
        ActivatePlayer(PlayerSelectionNumber);
    }

    public void goToPreviousPlayer()
    {
        PlayerSelectionNumber--;

        if(PlayerSelectionNumber < 0) {
            PlayerSelectionNumber = SelectablePlayers.Length - 1;
        }

        ActivatePlayer(PlayerSelectionNumber);
    }

    public void SetRacingMode(RacingMode racingMode)
    {
        if(racingMode == RacingMode.Racing)
        {
            dragonPropertyText.text = "Shenron: Summoned from balls";
            condorPropertyText.text = "Bald Bird: Only people with balls knows (if you know, you know)";
            chickenPropertyText.text = "KFC Escapee: buk, buk, buk, ba-gawk!!!";
        }

        if(racingMode == RacingMode.DeathRace)
        {
            dragonPropertyText.text = "Skill: Fireball \n Effect: Stuns for 2 seconds";
            condorPropertyText.text = "Skill: Dash \n Effect: Speeds up for 2 seconds";
            chickenPropertyText.text = "Skill: Egg Throw \n Effect: Slows multiple targets for 3 seconds";
        }
    }
}
