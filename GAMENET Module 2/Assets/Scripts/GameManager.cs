using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviourPunCallbacks
{
    #region Variables
    public static GameManager instance;
    
    public GameObject playerPrefab;
    public List<Transform> spawnPts;

    public GameObject globalUI;
    public Text killFeed;
    public GameObject endGameScreen;

    private Dictionary<string, int> playerKills = new Dictionary<string, int>();
   
    public bool isGameOver;
    #endregion

    #region Unity Functions
    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            Vector3 spawnPt = GetSpawnPt();
            PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(spawnPt.x, 0, spawnPt.z), Quaternion.identity);

            isGameOver = false;
            endGameScreen.SetActive(false);
           
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach(int value in playerKills.Values)
        {
            if(value >= 10)
            {
                photonView.RPC("ShowEndScreen", RpcTarget.AllBuffered);
            }
        }
    }

    #endregion

    [PunRPC]
    private void ShowEndScreen()
    {
        isGameOver = true;
        endGameScreen.SetActive(true);
    }

    public void RecordKill(string playerNickname, string target)
    {
        if(playerKills.ContainsKey(playerNickname))
        {
            playerKills[playerNickname] += 1;           
        }
        else if(!playerKills.ContainsKey(playerNickname))
        {
            playerKills.Add(playerNickname, 1);
        }

        photonView.RPC("UpdateKillFeed", RpcTarget.AllBuffered, playerNickname, target);
        
    }

    [PunRPC]
    private void UpdateKillFeed(string playerNickname, string target)
    {
        Debug.Log(playerNickname + " Kills: " + target);
        killFeed.text = playerNickname + " killed " + target;
        Invoke("ClearKillFeed", 5.0f);
    }

    private void ClearKillFeed()
    {
        killFeed.text = " ";
    }

    public Vector3 GetSpawnPt()
    {
        int randPos = Random.Range(0, spawnPts.Count - 1);

        return new Vector3(spawnPts[randPos].position.x, 0, spawnPts[randPos].position.z);
    }
    
}


