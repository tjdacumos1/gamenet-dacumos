using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public enum StatusAilments
{
    Normal,
    Boosted,
    Stunned,
    Slowed,
}

public class Status : MonoBehaviour
{
    public StatusAilments currentStatus;
    public PlayerMovement playerMovement;
    public float initialMoveSpeed;
    public Text statusText;

    private void Start()
    {
        currentStatus = StatusAilments.Normal;
        playerMovement = GetComponent<PlayerMovement>();
        initialMoveSpeed = playerMovement.moveSpeed;
    }

    private void Update()
    {
        if(currentStatus == StatusAilments.Normal)
        {
            playerMovement.moveSpeed = 25f;
        }

        if(currentStatus == StatusAilments.Boosted)
        {
            playerMovement.moveSpeed = 25f * 2f;
        }

        if(currentStatus == StatusAilments.Slowed)
        {
            playerMovement.moveSpeed = 25f / 2f;
        }

        if(currentStatus == StatusAilments.Stunned)
        {
            playerMovement.moveSpeed = 0;
        }
    }
    
    public void SetStatusAilment(StatusAilments status, float duration)
    {
        
        gameObject.GetComponent<PhotonView>().RPC("StatusAilmentUpdate", RpcTarget.AllBuffered, status, duration);
    }

    [PunRPC]
    private void StatusAilmentUpdate(StatusAilments status, float duration, PhotonMessageInfo info)
    {
        if(status != StatusAilments.Normal)
        {
            currentStatus = status;
            StartCoroutine(statusDurationTimer(duration));
        }       
        Debug.Log(info.photonView.Owner.NickName + " got hit by: " + info.Sender.NickName + " effect: " + currentStatus.ToString());
    }

    IEnumerator statusDurationTimer(float duration)
    {
        if (GetComponent<PhotonView>().IsMine)
        {
            statusText.text = currentStatus.ToString();
        }

        yield return new WaitForSeconds(duration);

        statusText.text = " ";
        currentStatus = StatusAilments.Normal;
    }
}
