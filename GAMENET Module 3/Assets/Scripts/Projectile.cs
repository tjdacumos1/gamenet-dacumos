using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviourPunCallbacks
{
    public float damage;

    private void Start()
    {
        Debug.LogWarning(GetComponent<Collider>());    
    }

    public void SetProjectileDamage(float amount)
    {
        damage = amount;
    }

    private void OnTriggerEnter(Collider other)
    {
        Shooting hit = other.GetComponent<Shooting>();

        if (other.GetComponent<PhotonView>().ViewID != photonView.ViewID)
        {
            if (hit != null)
            {
                hit.TakeDamageFromExternal(damage);
            }
            Destroy(gameObject);
        }        
        Debug.LogWarning(other);
    }
}
