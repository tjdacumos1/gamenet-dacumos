using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : Skills
{
    protected override void ActivateSkill()
    {
        base.ActivateSkill();

        GameObject projectileGameObject = PhotonNetwork.Instantiate(projectilePrefab.name, projectileSpawnPoint.position, projectileSpawnPoint.rotation);
        Rigidbody projectileRb = projectileGameObject.GetComponent<Rigidbody>();
        
        Projectile projectile = projectileGameObject.GetComponent<Projectile>();

        if (projectile != null)
        {
            projectile.SetProjectileEffect(statusAilmentsEffect, duration);
        }

        projectileRb.AddForce(projectileGameObject.transform.forward * bulletSpeed, ForceMode.Impulse);
    }
}