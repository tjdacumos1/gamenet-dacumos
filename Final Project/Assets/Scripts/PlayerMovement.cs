using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 25f;
    public float rotationSpeed = 2f;

    public GameObject model;
    private Rigidbody rb;

    
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (ClassicRacingManager.instance.isGamePlaying)
        {
            MovementInput();
            VerticalRotation();
            HorizontalRotation();
        }
        
    }

    void MovementInput()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(horizontalInput, 0f, verticalInput);
        rb.MovePosition(rb.position + transform.TransformDirection(movement.normalized * moveSpeed * Time.deltaTime));

    }

    void HorizontalRotation()
    {
        //float horizontalInput = Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;
        float mouseX = Input.GetAxis("Mouse X");

        //float newRotation = 0;
        //if(horizontalInput > 0f)
        //{
        //    newRotation = 20f;
        //}
        //else if (horizontalInput < 0f)
        //{
        //    newRotation = -20f; 
        //}

        Vector3 currentRotation = transform.rotation.eulerAngles;
        float newRotationY = currentRotation.y + mouseX * rotationSpeed;

        Quaternion deltaRotationY = Quaternion.Euler(currentRotation.x, newRotationY, currentRotation.z);
        transform.rotation = deltaRotationY;

        //model.transform.rotation = Quaternion.Euler(-90, newRotation, 0);
    }

    void VerticalRotation()
    {    
        float mouseY = Input.GetAxis("Mouse Y");


        Vector3 currentRotation = transform.rotation.eulerAngles;
        float newRotationX = currentRotation.x - mouseY * rotationSpeed;

        if (newRotationX > 180f)
        {
            newRotationX -= 360f;
        }

        newRotationX = Mathf.Clamp(newRotationX, -60f, 60f);

        Quaternion deltaRotationY = Quaternion.Euler(newRotationX, currentRotation.y, currentRotation.z);
        transform.rotation = deltaRotationY;
    }

}