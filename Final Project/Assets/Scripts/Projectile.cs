using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviourPunCallbacks
{
    private StatusAilments effectToStatus;
    private float effectDuration;

    private void OnTriggerEnter(Collider other)
    {
        
        Status hit = other.GetComponent<Status>();
        
        if(other.CompareTag("Player"))
        {
            PhotonView otherPlayer;
            otherPlayer = other.GetComponent<PhotonView>();

            if (!otherPlayer.IsMine)
            {
                if (hit != null)
                {
                    hit.SetStatusAilment(effectToStatus, effectDuration);
                    
                }
                PhotonNetwork.Destroy(gameObject);
            }
        }
        else if(!other.CompareTag("Projectile"))
        {
            PhotonNetwork.Destroy(gameObject);
        }                  
    }

    public void SetProjectileEffect(StatusAilments effect, float duration)
    {
        effectToStatus = effect;
        effectDuration = duration;
        Debug.Log(effectToStatus + " = " + effectDuration);
    }
}
