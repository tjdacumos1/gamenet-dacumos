using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera playerCam;
    
    void Awake()
    {
        this.playerCam = transform.Find("PlayerCamera").GetComponent<Camera>();

        if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<Status>().enabled = false;
            GetComponent<Skills>().enabled = false;
        }

        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<Status>().enabled = true;
            GetComponent<Skills>().enabled = photonView.IsMine;
        }

        GetComponent<LapController>().enabled = photonView.IsMine;
        GetComponent<CountdownManager>().enabled = photonView.IsMine;
        GetComponent<PlayerMovement>().enabled = photonView.IsMine;
        playerCam.enabled = photonView.IsMine;
    }

    private void Start()
    {
        Debug.Log("My ID: " + GetComponent<PhotonView>().ViewID);
    }
}
