using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class PlayerNameInputManager : MonoBehaviour
{
    [SerializeField]
    private Button enterButton;

    public void SetPlayerName(string playerName)
    {
        
        if(string.IsNullOrEmpty(playerName))
        {
            enterButton.interactable = false;
            Debug.LogWarning("Player name is empty!");           
        }
        else
        {
            enterButton.interactable = true;
            PhotonNetwork.NickName = playerName;
        }

        
    }

}
