using ExitGames.Client.Photon;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using UnityEngine.UI;

public enum RaiseEventsCode
{
    WhoDiedEventCode = 0
}

public class DeathController : MonoBehaviourPunCallbacks
{
    private int playerPlacement;

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoDiedEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfKilledPlayer = (string)data[0];
            playerPlacement = (int)data[1];
            int viewId = (int)data[2];
            string killerNickname = (string)data[3];

            Debug.Log("EVENT WAS RAISED: " + nickNameOfKilledPlayer + " " + playerPlacement);

            GameObject orderUiText = DeathRaceManager.instance.finisherTextUi[playerPlacement - 1];
            orderUiText.SetActive(true);

            //Debug.LogError("Owner viewID: " + photonView.ViewID + " & Stored viewID: " + viewId);

            if (viewId == photonView.ViewID)
            {
                orderUiText.GetComponent<Text>().text = nickNameOfKilledPlayer + "(YOU) has Died!";
                orderUiText.GetComponent<Text>().color = Color.red;

                Debug.Log(playerPlacement + " " + nickNameOfKilledPlayer + "(YOU)");
            }
            else
            {
                orderUiText.GetComponent<Text>().text = nickNameOfKilledPlayer + " has Died!";
            }

            DeathRaceManager.instance.DeductOnePlayer();

            Debug.LogWarning("Current Player Count: " + DeathRaceManager.instance.playersLeft.ToString());

            if (DeathRaceManager.instance.playersLeft <= 1)
            {
                DeathRaceManager.instance.winnerTextUi.SetActive(true);

                Debug.LogError("Owner Nickname: " + photonView.Owner.NickName + " & Winner Nickname: " + killerNickname);

                if (photonView.Owner.NickName == killerNickname && !gameObject.GetComponent<Shooting>().isDead)
                {
                    DeathRaceManager.instance.winnerTextUi.GetComponent<Text>().text = "Congratulations!!! " + killerNickname + "(You) Win!!";
                    DeathRaceManager.instance.winnerTextUi.GetComponent<Text>().color = Color.green;
                }
                else
                {
                    DeathRaceManager.instance.winnerTextUi.GetComponent<Text>().text = "You Lose.. " + killerNickname + " is the Winner.";
                    DeathRaceManager.instance.winnerTextUi.GetComponent<Text>().color = Color.red;
                }
            }
    }
}
}
